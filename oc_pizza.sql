-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : mer. 01 sep. 2021 à 07:53
-- Version du serveur :  5.7.32
-- Version de PHP : 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `oc_pizza`
--

-- --------------------------------------------------------

--
-- Structure de la table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `cellphone` varchar(45) NOT NULL,
  `type` enum('client','gerant','pizzaiolo','livreur') NOT NULL,
  `adress_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `adress`
--

CREATE TABLE `adress` (
  `id` int(11) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `street` varchar(90) NOT NULL,
  `street_complement` varchar(90) DEFAULT NULL,
  `zip_code` varchar(5) NOT NULL,
  `city` varchar(45) NOT NULL,
  `complement` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `designation` varchar(90) NOT NULL,
  `price` float DEFAULT NULL,
  `type` enum('SUPPLEMENT','SANS','BASE TOMATE','BASE CREME','GLACE','BOISSON') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `article_stock`
--

CREATE TABLE `article_stock` (
  `article_id` int(11) NOT NULL,
  `pizzeria_name` varchar(45) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL,
  `designation` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ingredient_stock`
--

CREATE TABLE `ingredient_stock` (
  `ingredient_id` int(11) NOT NULL,
  `pizzeria_name` varchar(45) NOT NULL,
  `stock_unit` enum('KG','L','Units') NOT NULL,
  `quantity` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `price` float NOT NULL,
  `status` enum('RECEIVED','IN PROGRESS','READY','DELIVERING','FINISHED','CANCELED') NOT NULL,
  `delivery` tinyint(4) NOT NULL,
  `payment` tinyint(4) NOT NULL,
  `payment_method1` enum('ESPECE','CB','TR','CHEQUE','PAYPAL') DEFAULT NULL,
  `value_payment1` float DEFAULT NULL,
  `payment_method2` enum('ESPECE','CB','TR','CHEQUE','PAYPAL') DEFAULT NULL,
  `value_payment2` float DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `order_line`
--

CREATE TABLE `order_line` (
  `article_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pizzeria`
--

CREATE TABLE `pizzeria` (
  `name` varchar(45) NOT NULL,
  `cellphone` varchar(45) NOT NULL,
  `mail` varchar(45) NOT NULL,
  `adress_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `recipe_line`
--

CREATE TABLE `recipe_line` (
  `article_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `recipe_unit` enum('gr','unit') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `adress`
--
ALTER TABLE `adress`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `article_stock`
--
ALTER TABLE `article_stock`
  ADD PRIMARY KEY (`article_id`,`pizzeria_name`),
  ADD KEY `fk_article_has_pizzeria_pizzeria1_idx` (`pizzeria_name`),
  ADD KEY `fk_article_has_pizzeria_article1_idx` (`article_id`);

--
-- Index pour la table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ingredient_stock`
--
ALTER TABLE `ingredient_stock`
  ADD PRIMARY KEY (`ingredient_id`,`pizzeria_name`),
  ADD KEY `fk_ingredient_has_pizzeria_pizzeria1_idx` (`pizzeria_name`),
  ADD KEY `fk_ingredient_has_pizzeria_ingredient1_idx` (`ingredient_id`);

--
-- Index pour la table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_order_account1_idx` (`account_id`);

--
-- Index pour la table `order_line`
--
ALTER TABLE `order_line`
  ADD PRIMARY KEY (`article_id`,`order_id`),
  ADD KEY `fk_article_has_order_order1_idx` (`order_id`),
  ADD KEY `fk_article_has_order_article1_idx` (`article_id`);

--
-- Index pour la table `pizzeria`
--
ALTER TABLE `pizzeria`
  ADD PRIMARY KEY (`name`),
  ADD KEY `fk_pizzeria_adress_idx` (`adress_id`);

--
-- Index pour la table `recipe_line`
--
ALTER TABLE `recipe_line`
  ADD PRIMARY KEY (`article_id`,`ingredient_id`),
  ADD KEY `fk_article_has_ingredient_ingredient1_idx` (`ingredient_id`),
  ADD KEY `fk_article_has_ingredient_article1_idx` (`article_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `adress`
--
ALTER TABLE `adress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article_stock`
--
ALTER TABLE `article_stock`
  ADD CONSTRAINT `fk_article_has_pizzeria_article1` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_article_has_pizzeria_pizzeria1` FOREIGN KEY (`pizzeria_name`) REFERENCES `pizzeria` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `ingredient_stock`
--
ALTER TABLE `ingredient_stock`
  ADD CONSTRAINT `fk_ingredient_has_pizzeria_ingredient1` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ingredient_has_pizzeria_pizzeria1` FOREIGN KEY (`pizzeria_name`) REFERENCES `pizzeria` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_account1` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `order_line`
--
ALTER TABLE `order_line`
  ADD CONSTRAINT `fk_article_has_order_article1` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_article_has_order_order1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pizzeria`
--
ALTER TABLE `pizzeria`
  ADD CONSTRAINT `fk_pizzeria_adress` FOREIGN KEY (`adress_id`) REFERENCES `adress` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `recipe_line`
--
ALTER TABLE `recipe_line`
  ADD CONSTRAINT `fk_article_has_ingredient_article1` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_article_has_ingredient_ingredient1` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
