-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : mer. 01 sep. 2021 à 07:53
-- Version du serveur :  5.7.32
-- Version de PHP : 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `OCPizza`
--

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

CREATE TABLE `adresse` (
  `id` int(6) NOT NULL,
  `numero` int(4) DEFAULT NULL,
  `rue` varchar(100) NOT NULL,
  `complement_rue` varchar(100) DEFAULT NULL,
  `codePostal` int(5) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `complement` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `adresse`
--

INSERT INTO `adresse` (`id`, `numero`, `rue`, `complement_rue`, `codePostal`, `ville`, `complement`) VALUES
(1, 23, 'rue des champs', NULL, 77130, 'MONTEREAU FAULT YONNE', NULL),
(2, 10, 'rue de la liberté', 'appartement 2B', 77130, 'MAROLLES SUR SEINE', 'portail B'),
(3, NULL, 'rue des Granges', NULL, 89340, 'VILLENEUVE LA GUYARD', 'portail vert'),
(4, 3, 'rue des Plages', NULL, 77130, 'CANNES ECLUSE', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `id` int(10) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `prix` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id`, `designation`, `prix`) VALUES
(1, 'HAAGEN-DAZS VANILLE 400G', 9.9),
(2, 'HAAGEN-DAZS COOKIE 400G', 9.9),
(3, 'ICE TEA 33CL', 1.5),
(4, 'COCA ZERO 33CL', 1.5),
(5, 'SUPPLEMENT CHEVRE', 1.5),
(6, 'SUPPLEMENT POULET', 1.5),
(7, 'SUPPLEMENT FROMAGE', 1.5),
(8, 'SUPPLEMENT POMME DE TERRE', 1.5),
(9, 'SANS CHAMPIGNONS', NULL),
(10, 'SANS OIGNONS', NULL),
(11, '4 FROMAGES PETITE', 11.9),
(12, '4 FROMAGES GRANDE', 13.9),
(13, 'CALZONE PETITE', 9.9),
(14, 'CALZONE GRANDE', 12.9),
(15, 'SAVOYARDE PETITE', 10.5),
(16, 'SAVOYARDE GRANDE', 13.5);

-- --------------------------------------------------------

--
-- Structure de la table `article stock`
--

CREATE TABLE `article stock` (
  `id_pizzeria` varchar(50) NOT NULL,
  `id_articleStocké` int(10) NOT NULL,
  `quantité` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `article stock`
--

INSERT INTO `article stock` (`id_pizzeria`, `id_articleStocké`, `quantité`) VALUES
('OC PIZZA MONTEREAU', 3, 3),
('OC PIZZA MONTEREAU', 4, 10),
('OC PIZZA MONTEREAU', 1, 4),
('OC PIZZA MONTEREAU', 2, 5);

-- --------------------------------------------------------

--
-- Structure de la table `articleStocké`
--

CREATE TABLE `articleStocké` (
  `article_id` int(10) NOT NULL,
  `catégorie` enum('boisson','glace') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `articleStocké`
--

INSERT INTO `articleStocké` (`article_id`, `catégorie`) VALUES
(1, 'glace'),
(2, 'glace'),
(3, 'boisson'),
(4, 'boisson');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id` int(6) NOT NULL,
  `montant` float NOT NULL,
  `statut` enum('recue','en préparation','prête','livraison','terminée','annulée') NOT NULL,
  `livraison` tinyint(1) NOT NULL,
  `adresseLivraison_id` int(6) DEFAULT NULL,
  `paiement` tinyint(1) NOT NULL,
  `modePaiement1` enum('espece','CB','TR','cheque','paypal') DEFAULT NULL,
  `montantPaiement1` float DEFAULT NULL,
  `modePaiement2` enum('espece','CB','TR','cheque','paypal') DEFAULT NULL,
  `montantPaiement2` float DEFAULT NULL,
  `heure` datetime NOT NULL,
  `compte_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id`, `montant`, `statut`, `livraison`, `adresseLivraison_id`, `paiement`, `modePaiement1`, `montantPaiement1`, `modePaiement2`, `montantPaiement2`, `heure`, `compte_id`) VALUES
(1, 55, 'terminée', 0, NULL, 1, 'CB', 55, NULL, NULL, '2021-08-20 19:00:00', 1),
(2, 34, 'annulée', 1, 4, 0, NULL, NULL, NULL, NULL, '2021-08-23 13:28:40', 1);

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE `compte` (
  `id` int(6) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `type` enum('client','gerant','pizzaiolo','livreur') NOT NULL,
  `adresse_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `compte`
--

INSERT INTO `compte` (`id`, `nom`, `prenom`, `email`, `password`, `telephone`, `type`, `adresse_id`) VALUES
(1, 'PEREA', 'Adrien', 'adrien@perea.be', 'adrien123', '0678978978', 'client', 4),
(2, 'GUITTON', 'Rolland', 'guigui@gmail.com', 'guigui123', '0789898989', 'pizzaiolo', 2),
(3, 'CRAVI', 'Richard', 'cracra@gmail.com', 'cracra123', '0712345434', 'livreur', 3);

-- --------------------------------------------------------

--
-- Structure de la table `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int(6) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `conditionnement` enum('kg','gr','litre','unitaire') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ingredient`
--

INSERT INTO `ingredient` (`id`, `designation`, `conditionnement`) VALUES
(1, 'Sauce tomate', 'litre'),
(2, 'Mélange fromage', 'kg'),
(3, 'Chèvre', 'kg'),
(4, 'Olive', 'kg'),
(5, 'Crème fraiche', 'kg'),
(6, 'Bleu', 'kg'),
(7, 'Pomme de terre', 'kg'),
(8, 'Reblochon', 'kg'),
(9, 'Oeuf', 'unitaire'),
(10, 'Jambon', 'kg'),
(11, 'lardon', 'kg');

-- --------------------------------------------------------

--
-- Structure de la table `ingredient stock`
--

CREATE TABLE `ingredient stock` (
  `id_ingredient` int(10) NOT NULL,
  `id_pizzeria` varchar(50) NOT NULL,
  `quantite` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ingredient stock`
--

INSERT INTO `ingredient stock` (`id_ingredient`, `id_pizzeria`, `quantite`) VALUES
(6, 'OC PIZZA MONTEREAU', 2),
(3, 'OC PIZZA MONTEREAU', 2),
(5, 'OC PIZZA MONTEREAU', 4),
(2, 'OC PIZZA MONTEREAU', 20),
(9, 'OC PIZZA MONTEREAU', 72),
(4, 'OC PIZZA MONTEREAU', 3),
(7, 'OC PIZZA MONTEREAU', 6),
(8, 'OC PIZZA MONTEREAU', 4),
(1, 'OC PIZZA MONTEREAU', 4),
(10, 'OC PIZZA MONTEREAU', 4),
(11, 'OC PIZZA MONTEREAU', 5);

-- --------------------------------------------------------

--
-- Structure de la table `ligne de commande`
--

CREATE TABLE `ligne de commande` (
  `id_commande` int(10) NOT NULL,
  `id_article` int(10) NOT NULL,
  `quantité` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ligne de commande`
--

INSERT INTO `ligne de commande` (`id_commande`, `id_article`, `quantité`) VALUES
(1, 12, 1),
(1, 4, 2),
(1, 2, 1),
(1, 15, 1),
(1, 10, 1),
(2, 14, 1),
(2, 8, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ligne de recette`
--

CREATE TABLE `ligne de recette` (
  `id_pizza` int(10) NOT NULL,
  `id_ingrédient` int(10) NOT NULL,
  `quantité` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `modification`
--

CREATE TABLE `modification` (
  `article_id` int(10) NOT NULL,
  `categorie` enum('supplément','sans') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `modification`
--

INSERT INTO `modification` (`article_id`, `categorie`) VALUES
(5, 'supplément'),
(6, 'supplément'),
(7, 'supplément'),
(8, 'supplément'),
(9, 'sans'),
(10, 'sans');

-- --------------------------------------------------------

--
-- Structure de la table `pizza`
--

CREATE TABLE `pizza` (
  `article_id` int(10) NOT NULL,
  `base` enum('tomate','crème') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pizza`
--

INSERT INTO `pizza` (`article_id`, `base`) VALUES
(11, 'crème'),
(12, 'crème'),
(13, 'tomate'),
(14, 'tomate'),
(15, 'tomate'),
(16, 'tomate');

-- --------------------------------------------------------

--
-- Structure de la table `pizzeria`
--

CREATE TABLE `pizzeria` (
  `nom` varchar(50) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `adresse_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pizzeria`
--

INSERT INTO `pizzeria` (`nom`, `telephone`, `mail`, `adresse_id`) VALUES
('OC PIZZA MONTEREAU', '0123456789', 'montereau@oc-pizza.fr', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `article stock`
--
ALTER TABLE `article stock`
  ADD KEY `article_stock_id_article` (`id_articleStocké`),
  ADD KEY `article_stock_id_pizzeria` (`id_pizzeria`);

--
-- Index pour la table `articleStocké`
--
ALTER TABLE `articleStocké`
  ADD PRIMARY KEY (`article_id`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commande_adresse_livraison_id` (`adresseLivraison_id`),
  ADD KEY `commande_compte_id` (`compte_id`);

--
-- Index pour la table `compte`
--
ALTER TABLE `compte`
  ADD PRIMARY KEY (`id`),
  ADD KEY `compte_adresse_id` (`adresse_id`);

--
-- Index pour la table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ingredient stock`
--
ALTER TABLE `ingredient stock`
  ADD KEY `ingredient_stock_id_ingredient` (`id_ingredient`),
  ADD KEY `ingredient_stock_id_pizzeria` (`id_pizzeria`);

--
-- Index pour la table `ligne de commande`
--
ALTER TABLE `ligne de commande`
  ADD KEY `ligne_commande_id_commande_FK` (`id_commande`) USING BTREE,
  ADD KEY `ligne_commande_id_article_FK` (`id_article`) USING BTREE;

--
-- Index pour la table `ligne de recette`
--
ALTER TABLE `ligne de recette`
  ADD KEY `ligne_recette_id_ingredient` (`id_ingrédient`),
  ADD KEY `ligne_recette_id_pizza` (`id_pizza`);

--
-- Index pour la table `modification`
--
ALTER TABLE `modification`
  ADD PRIMARY KEY (`article_id`);

--
-- Index pour la table `pizza`
--
ALTER TABLE `pizza`
  ADD PRIMARY KEY (`article_id`);

--
-- Index pour la table `pizzeria`
--
ALTER TABLE `pizzeria`
  ADD PRIMARY KEY (`nom`),
  ADD KEY `pizzeria_adresse_id` (`adresse_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `adresse`
--
ALTER TABLE `adresse`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `compte`
--
ALTER TABLE `compte`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `modification`
--
ALTER TABLE `modification`
  MODIFY `article_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `pizza`
--
ALTER TABLE `pizza`
  MODIFY `article_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article stock`
--
ALTER TABLE `article stock`
  ADD CONSTRAINT `article_stock_id_article` FOREIGN KEY (`id_articleStocké`) REFERENCES `articleStocké` (`article_id`),
  ADD CONSTRAINT `article_stock_id_pizzeria` FOREIGN KEY (`id_pizzeria`) REFERENCES `pizzeria` (`nom`);

--
-- Contraintes pour la table `articleStocké`
--
ALTER TABLE `articleStocké`
  ADD CONSTRAINT `article_stocké_id_article` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `commande_adresse_livraison_id` FOREIGN KEY (`adresseLivraison_id`) REFERENCES `adresse` (`id`),
  ADD CONSTRAINT `commande_compte_id` FOREIGN KEY (`compte_id`) REFERENCES `compte` (`id`);

--
-- Contraintes pour la table `compte`
--
ALTER TABLE `compte`
  ADD CONSTRAINT `compte_adresse_id` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`id`);

--
-- Contraintes pour la table `ingredient stock`
--
ALTER TABLE `ingredient stock`
  ADD CONSTRAINT `ingredient_stock_id_ingredient` FOREIGN KEY (`id_ingredient`) REFERENCES `ingredient` (`id`),
  ADD CONSTRAINT `ingredient_stock_id_pizzeria` FOREIGN KEY (`id_pizzeria`) REFERENCES `pizzeria` (`nom`);

--
-- Contraintes pour la table `ligne de commande`
--
ALTER TABLE `ligne de commande`
  ADD CONSTRAINT `id_article_FK` FOREIGN KEY (`id_article`) REFERENCES `article` (`id`),
  ADD CONSTRAINT `id_commande_FK` FOREIGN KEY (`id_commande`) REFERENCES `commande` (`id`);

--
-- Contraintes pour la table `ligne de recette`
--
ALTER TABLE `ligne de recette`
  ADD CONSTRAINT `ligne_recette_id_ingredient` FOREIGN KEY (`id_ingrédient`) REFERENCES `ingredient` (`id`),
  ADD CONSTRAINT `ligne_recette_id_pizza` FOREIGN KEY (`id_pizza`) REFERENCES `pizza` (`article_id`);

--
-- Contraintes pour la table `modification`
--
ALTER TABLE `modification`
  ADD CONSTRAINT `modification_article_id` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);

--
-- Contraintes pour la table `pizza`
--
ALTER TABLE `pizza`
  ADD CONSTRAINT `pizza_article_id` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);

--
-- Contraintes pour la table `pizzeria`
--
ALTER TABLE `pizzeria`
  ADD CONSTRAINT `pizzeria_adresse_id` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
